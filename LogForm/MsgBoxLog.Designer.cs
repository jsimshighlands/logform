﻿namespace LogForm
{
    partial class MsgBoxLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstViewMsgBoxLog = new System.Windows.Forms.ListView();
            this.Title = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TimeStamp = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.User = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Result = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lstViewMsgBoxLog
            // 
            this.lstViewMsgBoxLog.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Title,
            this.TimeStamp,
            this.User,
            this.Result});
            this.lstViewMsgBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstViewMsgBoxLog.GridLines = true;
            this.lstViewMsgBoxLog.Location = new System.Drawing.Point(0, 0);
            this.lstViewMsgBoxLog.Name = "lstViewMsgBoxLog";
            this.lstViewMsgBoxLog.Size = new System.Drawing.Size(604, 269);
            this.lstViewMsgBoxLog.TabIndex = 0;
            this.lstViewMsgBoxLog.UseCompatibleStateImageBehavior = false;
            this.lstViewMsgBoxLog.View = System.Windows.Forms.View.Details;
            // 
            // Title
            // 
            this.Title.Text = "Title";
            this.Title.Width = 240;
            // 
            // TimeStamp
            // 
            this.TimeStamp.Text = "TimeStamp";
            this.TimeStamp.Width = 120;
            // 
            // User
            // 
            this.User.Text = "User";
            this.User.Width = 120;
            // 
            // Result
            // 
            this.Result.Text = "Result";
            this.Result.Width = 120;
            // 
            // MsgBoxLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(604, 269);
            this.Controls.Add(this.lstViewMsgBoxLog);
            this.Name = "MsgBoxLog";
            this.Text = "Message Box Log";
            this.Load += new System.EventHandler(this.MsgBoxLog_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstViewMsgBoxLog;
        private System.Windows.Forms.ColumnHeader Title;
        private System.Windows.Forms.ColumnHeader TimeStamp;
        private System.Windows.Forms.ColumnHeader User;
        private System.Windows.Forms.ColumnHeader Result;
    }
}