﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects;

namespace LogForm
{
    public partial class MsgBoxLog : Form
    {
        public MsgBoxLog()
        {
            InitializeComponent();
        }

        private void MsgBoxLog_Load(object sender, EventArgs e)
        {
            var loan = EncompassApplication.CurrentLoan;

            var logEntries = loan.Fields["CX.MSGBOX.TRIGGER.LOG"].Value.ToString().Split(new[] { "\n" }, StringSplitOptions.None);

            foreach (var entry in logEntries)
            {
                var log = new ListViewItem(entry.Split(new[] { "\t" }, StringSplitOptions.None));
                lstViewMsgBoxLog.Items.Add(log);
                log.EnsureVisible();
            }
        }
    }
}
