﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EllieMae.Encompass.Forms;
using EllieMae.Encompass.BusinessObjects;
using EllieMae.Encompass.Automation;

namespace LogForm
{
    public class LogForm : EllieMae.Encompass.Forms.Form
    {
        private MsgBoxLog msgBoxLog;

        private EllieMae.Encompass.Forms.Button btnMsgBoxLog = null;

        public override void CreateControls()
        {
            this.btnMsgBoxLog = (EllieMae.Encompass.Forms.Button)FindControl("btnMsgBoxLog");
            this.btnMsgBoxLog.Click += new EventHandler(btnMsgBoxLog_Click);
        }

        private void btnMsgBoxLog_Click(object sender, EventArgs e)
        {
            this.msgBoxLog = new MsgBoxLog();
            this.msgBoxLog.Show();
        }
    }
}
